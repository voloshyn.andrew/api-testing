import { GeneralNS } from "../src/namespace/GeneralNS";
import ConfigType = GeneralNS.ConfigType;

export const config: ConfigType = {
  baseUrl: "https://affiliate.profitsocial.com/",
  timeout: 10000,
};
