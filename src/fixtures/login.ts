import { LoginNS } from "../namespace/LoginNS";

export const negativeData: LoginNS.PositiveFixture[] = [
  {
    name: "Invalid user",
    sendData: {
      login: "test",
      password: "test",
    },
    expected: {
      status: 200,
      errorBody: {
        message: "FAIL",
        uiMessage: "Incorrect",
      },
    },
  },
];
