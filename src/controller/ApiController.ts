import { BaseController, ControllerOptions } from "school-helper/lib/controller";
import { ResponseData } from "school-helper/lib/component";
import { config } from "../../config/config";
import { LoginNS } from "../namespace/LoginNS";

export default class ApiController extends BaseController {
  private loginPath: string = "login";

  constructor(options: ControllerOptions = { baseUrl: config.baseUrl }) {
    super(options);
  }

  async loginError(data: LoginNS.SendBodyType): Promise<ResponseData<LoginNS.ErrorBodyType>> {
    return this.request
      .json(data)
      .post<LoginNS.ErrorBodyType>(this.loginPath);
  }
}
