import * as zod from "zod";

export default class LoginChecker {
  static get loginErrorSchema() {
    return zod.object({
      message: zod.string(),
      uiMessage: zod.string(),
    });
  }
}
