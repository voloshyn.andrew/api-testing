module.exports = {
  setupFilesAfterEnv: ["<rootDir>/config/jest.setup.ts"],
  transform: {
    "^.+\\.(ts)$": "ts-jest",
  },
  testRegex: "/test/.*.tsx?$",
};
